### Tests
* Jest
* React Testing Library (./src/PlaceOrder/\_\_tests\_\_/PlaceOrderForm.tsx)

### CI/CD
* Bitbucket Pipelines (./bitbucket-pipelines.yml)

### Hosting
* AWS S3 (https://frontendtest-bitsgap.s3.eu-north-1.amazonaws.com/index.html)