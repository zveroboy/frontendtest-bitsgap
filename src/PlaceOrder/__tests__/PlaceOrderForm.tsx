import React from 'react'
import { render, screen } from '@testing-library/react'
import { createStore, getStore, StoreProvider } from '../context'
import { PlaceOrderForm } from '../PlaceOrderForm'
import {
  TESTID_PROFIT_TARGET_AMOUNT,
  TESTID_PROFIT_TARGET_CANCEL,
  TESTID_PROFIT_TARGET_PROFIT,
  TESTID_PROFIT_TARGET_TRADE_PRICE,
} from '../components/TakeProfit/components/TakeProfitInputRow'
import userEvent from '@testing-library/user-event'
import { TESTID_SWITCH } from '../../components/Switch/Switch'
import { PROFIT_TARGET_PROFIT_STEP } from '../constants'
import assert from 'assert'
import { TESTID_PROJECTED_PROFIT } from '../components/TakeProfit/TakeProfit'

afterEach(jest.clearAllMocks)

const renderForm = () =>
  render(
    <StoreProvider>
      <PlaceOrderForm />
    </StoreProvider>,
  )

const BACKSPACE_KEY = '{backspace}'

describe('PlaceOrderForm', () => {
  beforeEach(createStore)

  describe('Form validation', () => {
    test('1. валидация всех инпутов Take profit происходит по сабмиту формы; 5. значение инпута Target price должно быть > 0', async () => {
      renderForm()
      userEvent.click(screen.getByTestId(TESTID_SWITCH))
      const profitTargetTradePrice1 = screen
        .getAllByTestId(TESTID_PROFIT_TARGET_TRADE_PRICE)[0]
        .querySelector('input')
      assert(profitTargetTradePrice1)
      const errorMessage = 'Price must be greater than 0'
      expect(profitTargetTradePrice1).toHaveValue('0')
      expect(screen.queryByText(errorMessage)).not.toBeInTheDocument()
      userEvent.click(screen.getByText('Buy BTC'))
      profitTargetTradePrice1.focus()
      expect(await screen.findByText(errorMessage)).toBeInTheDocument()
    })

    test("2. сумма значений инпутов Profit у всех profit target'ов не должна превышать 500%", async () => {
      renderForm()
      userEvent.click(screen.getByTestId(TESTID_SWITCH))
      userEvent.click(screen.getByText(/Add profit target/))

      const profitTargetProfit1 = screen
        .getAllByTestId(TESTID_PROFIT_TARGET_PROFIT)[0]
        .querySelector('input')
      assert(profitTargetProfit1)

      const errorMessage = 'Maximum profit sum is 500%'
      expect(screen.queryByText(errorMessage)).not.toBeInTheDocument()
      await userEvent.type(profitTargetProfit1, BACKSPACE_KEY + '499')
      userEvent.click(screen.getByText('Buy BTC'))
      profitTargetProfit1.focus()
      expect(await screen.findByText(errorMessage)).toBeInTheDocument()
    })

    test('3. значение инпута Profit должно быть >= 0.01', async () => {
      renderForm()
      userEvent.click(screen.getByTestId(TESTID_SWITCH))
      userEvent.click(screen.getByText(/Add profit target/))

      const profitTargetProfit1 = screen
        .getAllByTestId(TESTID_PROFIT_TARGET_PROFIT)[0]
        .querySelector('input')
      assert(profitTargetProfit1)

      const errorMessage = 'Minimum value is 0.01'
      expect(screen.queryByText(errorMessage)).not.toBeInTheDocument()
      await userEvent.type(profitTargetProfit1, BACKSPACE_KEY + '.0001')
      userEvent.click(screen.getByText('Buy BTC'))
      profitTargetProfit1.focus()
      expect(await screen.findByText(errorMessage)).toBeInTheDocument()
    })

    test('4. значение каждого инпута Profit должно быть больше предыдущего (верхнего) значения Profit', async () => {
      renderForm()
      userEvent.click(screen.getByTestId(TESTID_SWITCH))
      userEvent.click(screen.getByText(/Add profit target/))

      const profitTargetProfit2 = screen
        .getAllByTestId(TESTID_PROFIT_TARGET_PROFIT)[1]
        .querySelector('input')
      assert(profitTargetProfit2)

      const errorMessage =
        "Each target's profit should be greater than the previous one"
      expect(screen.queryByText(errorMessage)).not.toBeInTheDocument()
      await userEvent.type(profitTargetProfit2, BACKSPACE_KEY + '1')
      userEvent.click(screen.getByText('Buy BTC'))
      profitTargetProfit2.focus()
      expect(await screen.findByText(errorMessage)).toBeInTheDocument()
    })

    test("6. сумма значений инпутов Amount у всех profit target'ов не должна превышать 100%", async () => {
      renderForm()
      userEvent.click(screen.getByTestId(TESTID_SWITCH))
      const addProfitButton = screen.getByText(/Add profit target/)
      userEvent.click(addProfitButton)
      userEvent.click(addProfitButton)

      const profitTargetAmount1 = screen
        .getAllByTestId(TESTID_PROFIT_TARGET_AMOUNT)[0]
        .querySelector('input')
      assert(profitTargetAmount1)

      const errorMessage = '110 out of 100% selected. Please decrease by 10'
      expect(screen.queryByText(errorMessage)).not.toBeInTheDocument()
      await userEvent.type(profitTargetAmount1, BACKSPACE_KEY.repeat(2) + '70')
      userEvent.click(screen.getByText('Buy BTC'))
      profitTargetAmount1.focus()
      expect(await screen.findByText(errorMessage)).toBeInTheDocument()
    })
  })

  describe('Profit target creation', () => {
    test("1.a как только будут добавлены все 5 profit target'ов, кнопка Add profit target пропадает до тех пор, пока не будет удален хотя бы один profit target", () => {
      renderForm()
      userEvent.click(screen.getByTestId(TESTID_SWITCH))
      const addProfitButton = screen.getByText(/Add profit target/)
      userEvent.click(addProfitButton)
      userEvent.click(addProfitButton)
      userEvent.click(addProfitButton)
      userEvent.click(addProfitButton)
      expect(addProfitButton).not.toBeInTheDocument()
      userEvent.click(screen.getAllByTestId(TESTID_PROFIT_TARGET_CANCEL)[1])
      expect(screen.getByText(/Add profit target/)).toBeInTheDocument()
    })

    test('1.b при добавлении очередного profit target, значения его инпутов устанавливаются следующим образом:', async () => {
      renderForm()
      await userEvent.type(screen.getByLabelText('Price'), '10000')
      await userEvent.type(screen.getByLabelText('Amount'), '0.5')

      userEvent.click(screen.getByTestId(TESTID_SWITCH))
      const profitTargetProfitInput1 = screen
        .getAllByTestId(TESTID_PROFIT_TARGET_PROFIT)[0]
        .querySelector('input')
      assert(profitTargetProfitInput1)
      await userEvent.type(profitTargetProfitInput1, BACKSPACE_KEY + '10')
      userEvent.click(screen.getByText(/Add profit target/))
      const profitTargetProfitInput2 = screen
        .getAllByTestId(TESTID_PROFIT_TARGET_PROFIT)[1]
        .querySelector('input')
      expect(profitTargetProfitInput2).toHaveValue('12')
      expect(
        screen
          .getAllByTestId(TESTID_PROFIT_TARGET_TRADE_PRICE)[1]
          .querySelector('input'),
      ).toHaveValue('11200')
      expect(
        screen
          .getAllByTestId(TESTID_PROFIT_TARGET_AMOUNT)[1]
          .querySelector('input'),
      ).toHaveValue('20')
    })

    test('1.c если при добавлении очередного profit target сумма всех значений Amount становится > 100, то необходимо уменьшить значение наибольшего Amount так, чтобы сумма стала = 100.', async () => {
      renderForm()
      userEvent.click(screen.getByTestId(TESTID_SWITCH))
      const profitTargetAmountInput1 = screen
        .getAllByTestId(TESTID_PROFIT_TARGET_AMOUNT)[0]
        .querySelector('input')
      assert(profitTargetAmountInput1)
      expect(profitTargetAmountInput1).toHaveValue('100')
      await userEvent.type(
        profitTargetAmountInput1,
        BACKSPACE_KEY.repeat(3) + '90',
      )
      userEvent.click(screen.getByText(/Add profit target/))

      const profitTargetAmountInput2 = screen
        .getAllByTestId(TESTID_PROFIT_TARGET_AMOUNT)[1]
        .querySelector('input')
      expect(profitTargetAmountInput2).toHaveValue('20')
      expect(profitTargetAmountInput1).toHaveValue('80')
    })

    test('2. при клике на крестик справа от profit target, данный ряд удаляется', async () => {
      const placeOrderStore = getStore()
      placeOrderStore.addProfitTarget()
      placeOrderStore.addProfitTarget()
      placeOrderStore.addProfitTarget()
      renderForm()
      const profitTargetProfitAll = screen.getAllByTestId(
        TESTID_PROFIT_TARGET_PROFIT,
      )
      expect(profitTargetProfitAll).toHaveLength(3)
      expect(
        profitTargetProfitAll.find((el) =>
          el.querySelector('input[value="4"]'),
        ),
      ).toBeInTheDocument()
      userEvent.click(screen.getAllByTestId(TESTID_PROFIT_TARGET_CANCEL)[1])

      const profitTargetProfitAllAfterDelete = await screen.findAllByTestId(
        TESTID_PROFIT_TARGET_PROFIT,
      )
      expect(
        profitTargetProfitAllAfterDelete.find((el) =>
          el.querySelector('input[value="4"]'),
        ),
      ).toBeUndefined()
      expect(profitTargetProfitAllAfterDelete).toHaveLength(2)
    })

    test('3. т.к. между значениями инпутов Profit и Target price имеется связь, каждый зависимый инпут должен пересчитываться на событии Blur второго инпута', async () => {
      renderForm()
      await userEvent.type(screen.getByLabelText('Price'), '10000')
      await userEvent.type(screen.getByLabelText('Amount'), '0.5')
      userEvent.click(screen.getByTestId(TESTID_SWITCH))

      expect(screen.queryAllByTestId(TESTID_PROFIT_TARGET_PROFIT)).toHaveLength(
        1,
      )
      const profitTargetProfitInput = screen
        .getByTestId(TESTID_PROFIT_TARGET_PROFIT)
        .querySelector('input')
      assert(profitTargetProfitInput)

      const profitTargetTradePriceInput = screen
        .getByTestId(TESTID_PROFIT_TARGET_TRADE_PRICE)
        .querySelector('input')
      expect(profitTargetTradePriceInput).toHaveValue('10200')

      await userEvent.type(profitTargetProfitInput, BACKSPACE_KEY + '4')
      profitTargetProfitInput.blur()

      assert(profitTargetTradePriceInput)
      expect(profitTargetTradePriceInput).toHaveValue('10400')

      await userEvent.type(
        profitTargetTradePriceInput,
        BACKSPACE_KEY.repeat(5) + '10600',
      )
      profitTargetTradePriceInput.blur()

      expect(profitTargetProfitInput).toHaveValue('6')
    })

    test('4.a свитч Take profit должен стать интерактивным: при клике на него опция Take profit либо отключается, тогда форма принимает вид как на рисунке 1,', () => {
      const placeOrderStore = getStore()
      placeOrderStore.addProfitTarget()
      placeOrderStore.addProfitTarget()
      renderForm()
      expect(screen.getAllByTestId(TESTID_PROFIT_TARGET_PROFIT)).toHaveLength(2)
      userEvent.click(screen.getByTestId(TESTID_SWITCH))
      expect(screen.queryAllByTestId(TESTID_PROFIT_TARGET_PROFIT)).toHaveLength(
        0,
      )
    })

    test("4.b либо включается с одним profit target'ом. Значения инпутов Profit и Trade price при этом сразу должны быть рассчитаны, а значение Amount = 100%.", async () => {
      expect(screen.queryAllByTestId(TESTID_PROFIT_TARGET_PROFIT)).toHaveLength(
        0,
      )
      renderForm()
      await userEvent.type(screen.getByLabelText('Price'), '10000')
      await userEvent.type(screen.getByLabelText('Amount'), '0.5')
      userEvent.click(screen.getByTestId(TESTID_SWITCH))
      expect(screen.getAllByTestId(TESTID_PROFIT_TARGET_PROFIT)).toHaveLength(1)
      expect(
        screen.getByTestId(TESTID_PROFIT_TARGET_PROFIT).querySelector('input'),
      ).toHaveValue(PROFIT_TARGET_PROFIT_STEP + '')
      expect(
        screen
          .getByTestId(TESTID_PROFIT_TARGET_TRADE_PRICE)
          .querySelector('input'),
      ).toHaveValue('10200')
      expect(
        screen.getByTestId(TESTID_PROFIT_TARGET_AMOUNT).querySelector('input'),
      ).toHaveValue('100')
    })

    test('5. projected profit рассчитывается следующим образом', () => {
      const placeOrderStore = getStore()
      placeOrderStore.price = 10000
      placeOrderStore.amount = 0.5
      placeOrderStore.addProfitTarget()
      placeOrderStore.addProfitTarget()
      renderForm()

      expect(screen.getByTestId(TESTID_PROJECTED_PROFIT)).toHaveTextContent(
        '240',
      )
      userEvent.click(screen.getByText('Sell'))

      expect(screen.getByTestId(TESTID_PROJECTED_PROFIT)).toHaveTextContent(
        '-240',
      )
    })
  })
})
