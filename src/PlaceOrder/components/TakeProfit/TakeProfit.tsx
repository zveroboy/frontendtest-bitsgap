/* eslint @typescript-eslint/no-use-before-define: 0 */

import React from 'react'
import block from 'bem-cn-lite'
import { AddCircle } from '@material-ui/icons'

import { Switch, TextButton } from 'components'

import { PROFIT_TARGET_LENGTH_LIMIT, QUOTE_CURRENCY } from '../../constants'
import { OrderSide } from '../../model'
import './TakeProfit.scss'
import { observer } from 'mobx-react'
import { TakeProfitTitles } from './components/TakeProfitTitles'
import { TakeProfitInputRow } from './components/TakeProfitInputRow'
import { computed } from 'mobx'
import { useStore } from '../../hooks/useStore'

type Props = {
  orderSide: OrderSide
  // ...
}

const b = block('take-profit')

export const TESTID_PROJECTED_PROFIT = 'TESTID_PROJECTED_PROFIT'

const TakeProfit = observer(({ orderSide }: Props) => {
  const {
    price,
    takeProfit,
    profitTargets,
    addProfitTarget,
    clearProfitTarget,
  } = useStore()

  const projectedProfit = computed(() =>
    profitTargets.reduce((acc, { amount, tradePrice }) => {
      const delta =
        orderSide === 'buy' ? tradePrice - price : price - tradePrice
      return acc + (delta * amount) / 100
    }, 0),
  ).get()

  return (
    <div className={b()}>
      <div className={b('switch')}>
        <span>Take profit</span>
        <Switch
          checked={takeProfit}
          onChange={(value) => {
            !value && profitTargets.length
              ? clearProfitTarget()
              : addProfitTarget()
          }}
        />
      </div>

      {profitTargets.length > 0 && (
        <div className={b('content')}>
          <TakeProfitTitles orderSide={orderSide} />
          {profitTargets.map((profitTarget, i) => (
            <TakeProfitInputRow key={i} profitTarget={profitTarget} />
          ))}

          {profitTargets.length < 5 && (
            <TextButton
              className={b('add-button')}
              onClick={() => addProfitTarget()}
            >
              <AddCircle className={b('add-icon')} />
              <span>
                Add profit target {profitTargets.length + 1}/
                {PROFIT_TARGET_LENGTH_LIMIT}
              </span>
            </TextButton>
          )}

          <div className={b('projected-profit')}>
            <span className={b('projected-profit-title')}>
              Projected profit
            </span>
            <span className={b('projected-profit-value')}>
              <span data-testid={TESTID_PROJECTED_PROFIT}>
                {projectedProfit}
              </span>
              <span className={b('projected-profit-currency')}>
                {QUOTE_CURRENCY}
              </span>
            </span>
          </div>
        </div>
      )}
    </div>
  )
})

export { TakeProfit }
