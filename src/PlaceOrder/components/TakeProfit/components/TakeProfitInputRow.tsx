import React, { FC, useCallback } from 'react'
import block from 'bem-cn-lite'
import { observer } from 'mobx-react'
import { Cancel } from '@material-ui/icons'
import { computed } from 'mobx'

import { NumberInput } from '../../../../components'
import { QUOTE_CURRENCY } from '../../../constants'
import { useProfitTargetValidator } from '../../../hooks/useProfitTargetValidator'
import { ProfitTarget } from '../../../store/ProfitTarget'
import { useStore } from '../../../hooks/useStore'

const b = block('take-profit')

export const TESTID_PROFIT_TARGET_PROFIT = 'TESTID_PROFIT_TARGET_PROFIT'
export const TESTID_PROFIT_TARGET_TRADE_PRICE =
  'TESTID_PROFIT_TARGET_TRADE_PRICE'
export const TESTID_PROFIT_TARGET_AMOUNT = 'TESTID_PROFIT_TARGET_AMOUNT'
export const TESTID_PROFIT_TARGET_CANCEL = 'TESTID_PROFIT_TARGET_CANCEL'

type Props = { profitTarget: ProfitTarget }

export const TakeProfitInputRow: FC<Props> = observer(({ profitTarget }) => {
  const validator = useProfitTargetValidator()

  const profitValidationErrors = computed(() =>
    validator.validate('profit', profitTarget),
  ).get()
  const targetPriceValidationErrors = computed(() =>
    validator.validate('tradePrice', profitTarget),
  ).get()
  const amountValidationErrors = computed(() =>
    validator.validate('amount', profitTarget),
  ).get()

  const { validation, setValidation } = useStore()

  const profitChangeHandler = useCallback(
    (value: number | null) => {
      const profit = Number(value)
      const tradePrice = ProfitTarget.tradePriceFromProfit(
        profitTarget.orderStore.price,
        profit,
      )
      profitTarget.setProfit(profit)
      profitTarget.setTradePrice(tradePrice)
      setValidation(false)
    },
    [profitTarget, setValidation],
  )

  const targetPriceChangeHandler = useCallback(
    (value: number | null) => {
      const tradePrice = Number(value)
      const profit = ProfitTarget.profitFromTargetPrice(
        profitTarget.orderStore.price,
        tradePrice,
      )
      profitTarget.setTradePrice(tradePrice)
      profitTarget.setProfit(profit)
      setValidation(false)
    },
    [profitTarget, setValidation],
  )

  const amountChangeHandler = useCallback(
    (value: number | null) => {
      profitTarget.setAmount(Number(value))
      setValidation(false)
    },
    [profitTarget, setValidation],
  )

  return (
    <div className={b('inputs')}>
      <NumberInput
        value={profitTarget.profit}
        decimalScale={2}
        InputProps={{ endAdornment: '%' }}
        variant="underlined"
        onBlur={profitChangeHandler}
        error={validation && profitValidationErrors.join(', ')}
        data-testid={TESTID_PROFIT_TARGET_PROFIT}
      />
      <NumberInput
        value={profitTarget.tradePrice}
        decimalScale={2}
        InputProps={{ endAdornment: QUOTE_CURRENCY }}
        variant="underlined"
        onBlur={targetPriceChangeHandler}
        error={validation && targetPriceValidationErrors.join(', ')}
        data-testid={TESTID_PROFIT_TARGET_TRADE_PRICE}
      />
      <NumberInput
        value={profitTarget.amount}
        decimalScale={2}
        InputProps={{ endAdornment: '%' }}
        variant="underlined"
        onBlur={amountChangeHandler}
        error={validation && amountValidationErrors.join(', ')}
        data-testid={TESTID_PROFIT_TARGET_AMOUNT}
      />
      <div className={b('cancel-icon')}>
        <Cancel
          onClick={profitTarget.remove}
          data-testid={TESTID_PROFIT_TARGET_CANCEL}
        />
      </div>
    </div>
  )
})
