import React, { FC } from 'react'
import block from 'bem-cn-lite'
import { OrderSide } from '../../../model'
import { observer } from 'mobx-react'

const b = block('take-profit')

type Props = {
  orderSide: OrderSide
}

export const TakeProfitTitles: FC<Props> = observer(({ orderSide }) => {
  return (
    <div className={b('titles')}>
      <span>Profit</span>
      <span>Trade price</span>
      <span>Amount to {orderSide === 'buy' ? 'sell' : 'buy'}</span>
    </div>
  )
})
