import React, { createContext } from 'react'

import { PlaceOrderStore } from './store/PlaceOrderStore'
import { ProfitTargetFactory } from './store/ProfitTarget'

let store: PlaceOrderStore

export const createStore = (): void => {
  // @ts-ignore
  window.store = store
  store = new PlaceOrderStore(ProfitTargetFactory.create)
}
export const getStore = (): PlaceOrderStore => store

createStore()
export const storeContext = createContext(getStore())

export const StoreProvider: React.FC = ({ children }) => {
  return <storeContext.Provider value={store}>{children}</storeContext.Provider>
}
