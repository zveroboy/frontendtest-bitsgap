import { IValidator, Rule } from '../model'
import { PROFIT_TARGET_PERCENT_LIMIT } from '../constants'
import { useRef } from 'react'
import { ProfitTarget } from '../store/ProfitTarget'

export const profitRules: Rule<ProfitTarget>[] = [
  [
    ({ orderStore }: ProfitTarget) =>
      orderStore.profitTargets.reduce((acc, { profit }) => acc + profit, 0) <
      PROFIT_TARGET_PERCENT_LIMIT,
    'Maximum profit sum is 500%',
  ],
  [({ profit }: ProfitTarget) => profit >= 0.01, 'Minimum value is 0.01'],
  [
    (profitTarget: ProfitTarget) => {
      const idx = profitTarget.orderStore.profitTargets.findIndex(
        (pt) => pt === profitTarget,
      )
      return (
        idx < 1 ||
        profitTarget.orderStore.profitTargets[idx - 1].profit <
          profitTarget.profit
      )
    },
    "Each target's profit should be greater than the previous one",
  ],
]

export const tradePriceRules: Rule<ProfitTarget>[] = [
  [
    ({ tradePrice }: ProfitTarget) => tradePrice > 0,
    'Price must be greater than 0',
  ],
]

export const amountRules: Rule<ProfitTarget>[] = [
  [
    (profitTarget: ProfitTarget) =>
      profitTarget.orderStore.profitTargets.reduce(
        (acc, { amount }) => acc + amount,
        0,
      ) <= 100,
    (profitTarget: ProfitTarget) => {
      const reducedAmount = profitTarget.orderStore.profitTargets.reduce(
        (acc, { amount }) => acc + amount,
        0,
      )
      return `${reducedAmount} out of 100% selected. Please decrease by ${
        reducedAmount - 100
      }`
    },
  ],
]

export class ProfitTargetValidator implements IValidator<ProfitTarget> {
  constructor(public rules: IValidator<ProfitTarget>['rules']) {}

  validate(key: keyof ProfitTarget, profitTarget: ProfitTarget): string[] {
    const rulesByKey = this.rules[key]
    if (!Array.isArray(rulesByKey)) {
      return []
    }
    return rulesByKey.reduce<string[]>((errors, [ruleFn, message]) => {
      if (!ruleFn(profitTarget)) {
        const messageText =
          typeof message === 'function' ? message(profitTarget) : message
        errors.push(messageText)
      }
      return errors
    }, [])
  }
}

export const useProfitTargetValidator = () => {
  return useRef(
    new ProfitTargetValidator({
      profit: profitRules,
      tradePrice: tradePriceRules,
      amount: amountRules,
    }),
  ).current
}
