export type OrderSide = 'buy' | 'sell'

export type Rule<T> = [
  ruleFn: (validatable: T) => boolean,
  message: string | ((validatable: T) => string),
]

export interface IValidator<T> {
  rules: { [K in keyof T]?: Rule<T>[] }
  validate(key: string, validatable: T): string[]
}
