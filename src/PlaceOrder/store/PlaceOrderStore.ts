import { action, computed, observable } from 'mobx'

import { OrderSide } from '../model'
import { ProfitTarget } from './ProfitTarget'

export class PlaceOrderStore {
  @observable activeOrderSide: OrderSide = 'buy'
  @observable price: number = 0
  @observable amount: number = 0
  @observable profitTargets: ProfitTarget[] = []

  constructor(
    private profitTargetFactory: (
      orderStore: PlaceOrderStore,
    ) => ProfitTarget | null,
  ) {}

  @action.bound
  addProfitTarget(): void {
    const profitTarget = this.profitTargetFactory(this)
    profitTarget && this.profitTargets.push(profitTarget)
  }

  @action.bound
  removeProfitTarget(profitTarget: ProfitTarget): void {
    this.profitTargets.splice(this.profitTargets.indexOf(profitTarget), 1)
  }

  @action.bound
  clearProfitTarget(): void {
    this.profitTargets.length = 0
  }

  @computed get takeProfit(): boolean {
    return this.profitTargets.length > 0
  }

  @computed get total(): number {
    return this.price * this.amount
  }

  @action.bound
  public setOrderSide(side: OrderSide) {
    this.activeOrderSide = side
  }

  @action.bound
  public setPrice(price: number) {
    this.price = price
  }

  @action.bound
  public setAmount(amount: number) {
    this.amount = amount
  }

  @action.bound
  public setTotal(total: number) {
    this.amount = this.price > 0 ? total / this.price : 0
  }

  @observable validation: boolean = false
  @action.bound
  setValidation(value: boolean) {
    this.validation = value
  }
}

// Object.values(this.rules)
//   .filter<Rule<ProfitTarget>[]>(Array.isArray)
//   .flatMap((rules) => rules)
