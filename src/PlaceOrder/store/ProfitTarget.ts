import {
  PROFIT_TARGET_DEFAULT_AMOUNT,
  PROFIT_TARGET_LENGTH_LIMIT,
  PROFIT_TARGET_PROFIT_STEP,
} from '../constants'
import { add, find, last, pluck, propEq } from 'ramda'
import { action, observable } from 'mobx'

import { PlaceOrderStore } from './PlaceOrderStore'

export class ProfitTarget {
  static tradePriceFromProfit(price: number, profit: number): number {
    return price + (price * profit) / 100
  }

  static profitFromTargetPrice(price: number, targetPrice: number): number {
    const number = ((targetPrice - price) * 100) / price
    return isFinite(number) ? number : 0
  }

  @observable profit: number = 0
  @observable tradePrice: number = 0
  @observable amount: number = 0

  constructor(public orderStore: PlaceOrderStore) {}

  @action.bound
  remove() {
    this.orderStore.removeProfitTarget(this)
  }

  @action.bound
  setProfit(value: number) {
    this.profit = value
  }

  @action.bound
  setTradePrice(value: number) {
    this.tradePrice = value
  }

  @action.bound
  setAmount(value: number) {
    this.amount = value
  }
}

export class ProfitTargetFactory {
  static create(orderStore: PlaceOrderStore): ProfitTarget | null {
    if (orderStore.profitTargets.length >= PROFIT_TARGET_LENGTH_LIMIT) {
      return null
    }

    const profitTarget = new ProfitTarget(orderStore)
    const prevProfit = last(orderStore.profitTargets)?.profit
    profitTarget.profit = (prevProfit ?? 0) + PROFIT_TARGET_PROFIT_STEP
    profitTarget.tradePrice = ProfitTarget.tradePriceFromProfit(
      orderStore.price,
      profitTarget.profit,
    )
    profitTarget.amount = orderStore.profitTargets.length
      ? PROFIT_TARGET_DEFAULT_AMOUNT
      : 100

    ProfitTargetFactory.limitAmount([...orderStore.profitTargets, profitTarget])

    return profitTarget
  }

  private static limitAmount(profitTargets: ProfitTarget[]) {
    const amounts = pluck('amount', profitTargets)
    const reducedAmount = amounts.reduce(add)
    if (reducedAmount > 100) {
      const maxAmount = Math.max(...amounts)
      const profitTargetWMaxAmount = find(propEq('amount', maxAmount))(
        profitTargets,
      )!
      profitTargetWMaxAmount.amount += 100 - reducedAmount
    }
  }
}
